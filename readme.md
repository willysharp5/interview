# Take-Home Coding Challenge Examples

Take-home coding challenge workshop coding challenges live here.

## Front-End 

[Front-end challenge](front-end): Build a responsive navbar with login/logout.

## Back-End

[Back-end challenge](back-end): Build a RESTful API for conference talks and attendees.

